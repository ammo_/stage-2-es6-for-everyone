import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {

  Object.assign(firstFighter , {blockStatus : false , criticalStrikeTime : new Date().getTime()})
  Object.assign(secondFighter , {blockStatus : false , criticalStrikeTime : new Date().getTime()})

  document.getElementById('left-fighter-indicator').setAttribute('health' , firstFighter.health)
  document.getElementById('left-fighter-indicator').setAttribute('current-health' , firstFighter.health)

  document.getElementById('right-fighter-indicator').setAttribute('health' , secondFighter.health)
  document.getElementById('right-fighter-indicator').setAttribute('current-health' , secondFighter.health)

  window.firstFighter = firstFighter
  window.secondFighter = secondFighter

  return makeKick(firstFighter, secondFighter)
      .then( res => {
        return res
      })

}

export function getDamage(attacker, defender) {
  return getHitPower(attacker) - getBlockPower(defender)
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = Math.random() * (2 - 1 + 1) + 1
  return fighter.attack * criticalHitChance
}

export function getBlockPower(fighter) {
  // return block power
  if (fighter.blockStatus){
    let dodgeChance = Math.random() * (2 - 1 + 1) + 1
    return fighter.defense * dodgeChance
  }
  else {
    return 0
  }

}

function runOnKeys(func, ...codes) {
  let pressed = new Set();

  document.addEventListener('keydown', function (event) {
    pressed.add(event.code);

    for (let code of codes) { // все ли клавиши из набора нажаты?
      if (!pressed.has(code)) {
        return;
      }
    }
    pressed.clear();

    func();

  });

}
// first player -> critical strike
runOnKeys(
    () => {
      if (new Date().getTime() > window.firstFighter.criticalStrikeTime){
        changeHealthBar('right-fighter-indicator', window.firstFighter.attack * 2)
        window.firstFighter.criticalStrikeTime = new Date().getTime() + 10 * 1000
      }
    },
    'KeyQ', 'KeyW', 'KeyE'
);
// Second player -> critical strike
runOnKeys(
    () => {
      if (new Date().getTime() > window.secondFighter.criticalStrikeTime) {
        changeHealthBar('left-fighter-indicator', window.secondFighter.attack * 2)
        window.secondFighter.criticalStrikeTime = new Date().getTime() + 10 * 1000
      }

    },
    'KeyU', 'KeyI', 'KeyO'
);


async function makeKick(firstFighter, secondFighter) {

    function handlerOnDown() {

      switch (event.code) {
        case 'KeyD' :
          firstFighter.blockStatus = true
        case 'KeyL' :
          secondFighter.blockStatus = true
      }

    }

    function handlerOnUp() {

      switch (event.code) {

        case 'KeyA' :
          //kick -> playser 1
          if (secondFighter.blockStatus != true){
            changeHealthBar('right-fighter-indicator', getDamage(firstFighter, secondFighter))
          }

          break
          //kick -> playser 2
        case 'KeyJ' :
          //kick -> playser 1
            if ( firstFighter.blockStatus != true ){
              changeHealthBar('left-fighter-indicator', getDamage(secondFighter, firstFighter))
            }

          break
        case 'KeyD' :
          //block -> playser 1
          firstFighter.blockStatus = false
        case 'KeyL' :
          //block -> playser 2
          secondFighter.blockStatus = false
      }

    }

    document.addEventListener('keyup', handlerOnUp, false)
    document.addEventListener('keydown', handlerOnDown, false)

    return new Promise((resolve) => {

      setInterval(() => {
        if (+document.getElementById('left-fighter-indicator').getAttribute('current-health') <= 0) {
          resolve(window.secondFighter)
        }
        if (+document.getElementById('right-fighter-indicator').getAttribute('current-health') <= 0) {
          resolve(window.firstFighter)
        }
      }, 100)
    })
  }

  function changeHealthBar(fighterBar, damage) {

    let el = document.getElementById(fighterBar),
        totalHealth = el.getAttribute('health'),
        currentHealth = el.getAttribute('current-health'),
        onePercent = totalHealth / 100

    if (damage >= 0 && currentHealth > 0) {

      if (currentHealth - damage < 0) {
        el.setAttribute('current-health', 0)
        el.style.width = 0 + '%'
      } else {
        el.setAttribute('current-health', currentHealth - damage)
        el.style.width = (currentHealth - damage) / onePercent + '%'
      }

    }

  }
