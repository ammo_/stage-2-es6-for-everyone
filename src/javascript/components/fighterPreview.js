import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  console.log(fighter);
  console.log(position);
  // todo: show fighter info (image, name, health, etc.)

  if ( fighter ){
    fighterElement.innerHTML = `
    <img class="hero_image" src="${fighter.source}" alt="">
    <h1>${fighter.name}</h1>
    <table>
        <tr>
            <td>attack</td>
            <td>${fighter.attack}</td>
        </tr>
        <tr>
            <td>defense</td>
            <td>${fighter.defense}</td>
        </tr>
        <tr>
            <td>health</td>
            <td>${fighter.health}</td>
        </tr>
    </table>
    <style>
        .hero_image {
            height: 350px;
        }
        h1 , table{
            margin: 0;
            width: 100%;
        }
        h1, tr, td { 
            color : #fff;
            font-family: sans-serif;
            font-weight: 700;
        }
    </style>
  `
  }


  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
